/*
 * (c) 2014 forcare BV
 * All rights reserved.
 */
package fhir;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;

import nl.forcare.common.test.Persona;
import nl.forcare.core.CharsetUtil;
import nl.forcare.core.Oid;
import nl.forcare.datatypes.PatientId;
import nl.forcare.hl7.structures.datatypes.DTM;
import nl.forcare.xds.bo.Author;
import nl.forcare.xds.bo.AuthorInstitution;
import nl.forcare.xds.bo.AuthorRole;
import nl.forcare.xds.bo.AuthorSpecialty;
import nl.forcare.xds.bo.AuthorTelecom;
import nl.forcare.xds.bo.Document;
import nl.forcare.xds.bo.SubmissionSet;
import nl.forcare.xds.bo.test.DocumentTestUtil;
import nl.forcare.xds.bo.test.SubmissionSetTestUtil;

import org.hl7.fhir.instance.formats.JsonComposer;
import org.hl7.fhir.instance.formats.XmlComposer;
import org.hl7.fhir.instance.model.AtomCategory;
import org.hl7.fhir.instance.model.AtomEntry;
import org.hl7.fhir.instance.model.AtomFeed;
import org.hl7.fhir.instance.model.Binary;
import org.hl7.fhir.instance.model.Code;
import org.hl7.fhir.instance.model.CodeableConcept;
import org.hl7.fhir.instance.model.Coding;
import org.hl7.fhir.instance.model.DateAndTime;
import org.hl7.fhir.instance.model.DocumentManifest;
import org.hl7.fhir.instance.model.DocumentReference;
import org.hl7.fhir.instance.model.Id;
import org.hl7.fhir.instance.model.Identifier;
import org.hl7.fhir.instance.model.Identifier.IdentifierUse;
import org.hl7.fhir.instance.model.Instant;
import org.hl7.fhir.instance.model.MessageHeader;
import org.hl7.fhir.instance.model.MessageHeader.MessageSourceComponent;
import org.hl7.fhir.instance.model.Narrative;
import org.hl7.fhir.instance.model.Narrative.NarrativeStatus;
import org.hl7.fhir.instance.model.OperationOutcome;
import org.hl7.fhir.instance.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.instance.model.OperationOutcome.OperationOutcomeIssueComponent;
import org.hl7.fhir.instance.model.Patient;
import org.hl7.fhir.instance.model.Practitioner;
import org.hl7.fhir.instance.model.Resource;
import org.hl7.fhir.instance.model.ResourceReference;
import org.hl7.fhir.instance.model.String_;
import org.hl7.fhir.instance.model.Uri;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;
import org.junit.Test;

public class ProvideDocumentResourcesTest {
	@Test
	public void testShouldYieldPrettyResponsesToo() throws Exception {
		OperationOutcome outcome = new OperationOutcome();
		String content = "failed to process transaction.";
		Narrative text = createNarrative(content);
		OperationOutcomeIssueComponent issue = outcome.addIssue();
		issue.setSeveritySimple(IssueSeverity.error);
		String_ value = new String_();
		value.setValue("Incorrect document syntax.");
		issue.setDetails(value);
		outcome.setText(text);

		JsonComposer composer = new JsonComposer();
		OutputStream jsonOs = new FileOutputStream("operationOutcome.json");
		composer.compose(jsonOs, outcome, true);

		XmlComposer xmlComposer = new XmlComposer();
		OutputStream xmlOs = new FileOutputStream("operationOutcome.xml");
		xmlComposer.compose(xmlOs, outcome, true);

	}

	private Narrative createNarrative(String content) {
		Narrative text = new Narrative();
		XhtmlNode node = new XhtmlNode();
		node.setNodeType(NodeType.Element);
		node.setName("p");
		node.addText(content);
		text.setDiv(node);
		text.setStatusSimple(NarrativeStatus.generated);
		return text;
	}

	@Test
	public void testShouldYieldPrettyJsonAndXmlRepresentations() throws Exception {
		AtomFeed feed = new AtomFeed();
		feed.setTitle("example submission bundle");

		feed.getTags().add(
				new AtomCategory("http://hl7.org/fhir/tag", "http://ihe.net/fhir/tag/iti-65", null));
		feed.setId(generateUuid());

		// createHeader();

		Document doc1 = DocumentTestUtil.createNewDocument("doc1", Persona.LISA_JENSEN);
		doc1.setSourcePatientId("6578946^^^&1.2.3&ISO");
		doc1.setURI("https://somehost/mhd2/Binary/" + doc1.getEntryUuid());
		Document doc2 = DocumentTestUtil.createNewDocument("doc2", Persona.LISA_JENSEN);
		doc2.setSourcePatientId("6578946^^^&1.2.3&ISO");
		doc2.setURI(null);
		SubmissionSet xdsSubSet = SubmissionSetTestUtil.createNewSubset(doc1, doc2);
		AtomEntry<Resource> subset = createDocumentManifest(xdsSubSet);

		feed.getEntryList().add(subset);

		int i = 0;
		for (Document doc : xdsSubSet.getDocuments()) {
			feed.getEntryList().add(createEntry(createDocumentReference("doc" + i++, doc), doc));
		}

		createRepresentations(feed, "ProvideDocumentResourcesExampleSkeleton");
	}

	private AtomEntry<Resource> createDocumentManifest(SubmissionSet submissionSet) {
		AtomEntry<Resource> subset = new AtomEntry<>();
		subset.setResource(createManifest(submissionSet));
		subset.setTitle(submissionSet.getTitle());
		return subset;
	}

	private void createHeader() {
		AtomEntry<Resource> header = new AtomEntry<>();
		Instant timestamp = new Instant();
		timestamp.setValue(new DateAndTime(new Date()));
		Coding event = new Coding();
		Code code = new Code();
		code.setValue("ITI-65");
		event.setCode(code);
		Uri value = new Uri();
		value.setValue("urn:ihe:iti");
		event.setSystem(value);
		Id id = new Id();
		id.setValue("msg1");
		header.setResource(new MessageHeader(id, timestamp, event, new MessageSourceComponent()));
	}

	private void createRepresentations(AtomFeed feed, String filename)
			throws FileNotFoundException, Exception {
		createJson(feed, filename + ".json");
		createXml(feed, filename + ".xml");
	}

	private void createXml(AtomFeed feed, String filename) throws FileNotFoundException, Exception {
		XmlComposer xmlComposer = new XmlComposer();
		OutputStream xmlOs = new FileOutputStream(filename);
		xmlComposer.compose(xmlOs, feed, true);
	}

	private void createJson(AtomFeed feed, String filename) throws FileNotFoundException, Exception {
		JsonComposer composer = new JsonComposer();
		OutputStream jsonOs = new FileOutputStream(filename);
		composer.compose(jsonOs, feed, true);
	}

	private AtomEntry<Resource> createEntry(DocumentReference resource, Document doc)
			throws ParseException {
		AtomEntry<Resource> entry = new AtomEntry<>();
		entry.setId(generateUuid());
		Patient e = new Patient();
		e.setXmlId("patient");
		PatientId patientId = doc.getPatientId();
		populateIdentifier(e.addIdentifier(), patientId, IdentifierUse.official);
		populateIdentifier(e.addIdentifier(),
				PatientId.fromString(doc.getSourcePatientId(), false), IdentifierUse.usual);

		resource.getContained().add(e);
		entry.setResource(resource);
		entry.setTitle("example DocumentEntry");
		return entry;
	}

	private Identifier populateIdentifier(Identifier identifier, PatientId patientId,
			IdentifierUse use) {
		identifier.setUseSimple(use);
		identifier.setValueSimple(patientId.getId());
		identifier.setSystemSimple("urn:oid:" + patientId.getAuthorityId().toString());
		return identifier;
	}

	private String generateUuid() {
		return "urn:uuid:" + UUID.randomUUID();
	}

	private DocumentReference createDocumentReference(String id, Document doc) {
		DocumentReference resource = new DocumentReference();
		resource.setLanguageSimple(doc.getLanguageCode());
		resource.setHashSimple(doc.getHash());
		resource.setDescriptionSimple(doc.getTitle());
		resource.setMimeTypeSimple(doc.getMimeType());
		resource.addFormatSimple("urn:ihe:xds:formatCode:"
				+ doc.getFormatCode().createStoredQueryRepresentation());
		resource.setText(createNarrative(doc.getComments()));
		resource.setType(createCodeableConcept(doc.getTypeCode()));
		resource.setClass_(createCodeableConcept(doc.getClassCode()));
		DTM dtm = doc.getCreationTime();
		resource.setCreatedSimple(createDateAndTime(dtm));
		resource.setSubject(createResourceReference("patient"));
		byte[] content = new String("this is my document.").getBytes(CharsetUtil.UTF8);
		resource.setSizeSimple(content.length);

		Identifier identifier = resource.addIdentifier();
		identifier.setValueSimple(doc.getUniqueId());
		for (Author author : doc.getAuthors()) {
			Resource pract = createPractitioner(author);
			resource.getContained().add(pract);
			ResourceReference ref = resource.addAuthor();
			ref.setReferenceSimple("#" + pract.getXmlId());
		}

		if (doc.getURI() != null) {
			resource.setLocationSimple(doc.getURI());
		} else {
			Binary bin = createBinary(id, content);
			resource.getContained().add(bin);
		}
		
		return resource;
	}

	private Binary createBinary(String id, byte[] content) {
		Binary bin = new Binary();
		bin.setXmlId(id);
		bin.setContentType("text/plain");
		bin.setContent(content);
		return bin;
	}

	private DateAndTime createDateAndTime(DTM dtm) {
		return new DateAndTime(dtm.getDateWithPrecision().getDatetime());
	}

	private CodeableConcept createCodeableConcept(nl.forcare.xds.bo.Code code) {
		CodeableConcept codeableConcept = new CodeableConcept();
		Coding coding = codeableConcept.addCoding();
		coding.setDisplaySimple(code.getDisplayName());
		coding.setCodeSimple(code.getValue());
		coding.setSystemSimple(code.getCodingScheme());
		return codeableConcept;
	}

	private ResourceReference createResourceReference(String value) {
		ResourceReference ref = new ResourceReference();
		ref.setReferenceSimple("#" + value);
		return ref;
	}

	private DocumentManifest createManifest(SubmissionSet submissionSet) {
		// author
		// comments
		// sourceId uniqueid entryuuid, contenttypecode, intendedrecipient, submissiontime

		DocumentManifest manifest = new DocumentManifest();
		Oid subsetUniqueId = submissionSet.getUniqueId();
		manifest.setSourceSimple("urn:oid:" + submissionSet.getSourceId());
		manifest.setText(createNarrative(submissionSet.getComments()));
		manifest.setMasterIdentifier(createUniqueId(subsetUniqueId));
		ResourceReference ref = manifest.addSubject();
		ref.setReferenceSimple("#patient");
		for (Author author : submissionSet.getAuthors()) {
			manifest.getContained().add(createPractitioner(author));
		}
		return manifest;
	}

	private Resource createPractitioner(Author author) {
		Practitioner pract = new Practitioner();
		for (AuthorRole role : author.getRoles()) {
			pract.addRole().setTextSimple(role.getName());
		}

		for (AuthorSpecialty specialty : author.getSpecialties()) {
			pract.addSpecialty().setTextSimple(specialty.getName());
		}

		for (AuthorInstitution institutions : author.getInstitutions()) {
			// no mapping!
		}

		for (AuthorTelecom telecom : author.getTelecom()) {
			pract.addTelecom().setValueSimple(telecom.toXTN().toString());
		}

		pract.setXmlId(author.getPerson().toXCN().getIdNumber());
		return pract;
	}

	private Identifier createUniqueId(Oid subsetUniqueId) {
		Identifier uniqueId = new Identifier();
		uniqueId.setValueSimple(subsetUniqueId.toString());
		return uniqueId;
	}
}
